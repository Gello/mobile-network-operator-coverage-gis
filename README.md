# Mobile network operator coverage maps for GIS (+OsmAnd)

## Operator's official maps

### MTS

	https://tiles.qsupport.mts.ru/TYPE/ZOOM/X/Y/

* TYPE
  - g2_New (2G)
  - g3_New (3G)
  - lte_New (4G)
* ZOOM [7..12]
* Projection — Merkaartor

#### OsmAnd

| Type | URL                                                | Min Zoom | Max Zoom | Merkaartor |
| ---- | -------------------------------------------------- | -------- | -------- | ---------- |
|  2G  | https://tiles.qsupport.mts.ru/g2_New/{0}/{1}/{2}/  | 7        | 12       | True       |
|  3G  | https://tiles.qsupport.mts.ru/g3_New/{0}/{1}/{2}/  | 7        | 12       | True       |
|  LTE | https://tiles.qsupport.mts.ru/lte_New/{0}/{1}/{2}/ | 7        | 12       | True       |



### Beeline

	https://static.beeline.ru/upload/tiles/TYPE/current/ZOOM/X/Y.png

* TYPE
  - 2G
  - 3G
  - 4G
* ZOOM [2..16]

#### OsmAnd

| Type | URL                                                                | Min Zoom | Max Zoom | Merkaartor |
| ---- | ------------------------------------------------------------------ | -------- | -------- | ---------- |
|  2G  | https://static.beeline.ru/upload/tiles/2G/current/{0}/{1}/{2}.png  | 2        | 16       | False      |
|  3G  | https://static.beeline.ru/upload/tiles/3G/current/{0}/{1}/{2}.png  | 2        | 16       | False      |
|  LTE | https://static.beeline.ru/upload/tiles/4G/current/{0}/{1}/{2}.png  | 2        | 16       | False      |



### MegaFon

	https://coverage-map.megafon.ru/ZOOM/X/Y.png?layers=TYPE

* TYPE
  - 2g (2G)
  - 3g (3G)
  - lte (4G up to 150 Mbit/s)
  - lte_plus (4G+ up to 300 Mbit/s)
* ZOOM [0..13]
* Projection — Merkaartor

All types can be combined with comma: `2g.3g`, `lte.lte_plus`, `2g.3g.lte.lte_plus`…

#### OsmAnd

| Type  | URL                                                                       | Min Zoom | Max Zoom | Merkaartor |
| ----- | ------------------------------------------------------------------------- | -------- | -------- | ---------- |
|  2G   | https://coverage-map.megafon.ru/{0}/{1}/{2}.png?layers=2g                 | 0        | 13       | True       |
|  3G   | https://coverage-map.megafon.ru/{0}/{1}/{2}.png?layers=3g                 | 0        | 13       | True       |
|  LTE  | https://coverage-map.megafon.ru/{0}/{1}/{2}.png?layers=lte                | 0        | 13       | True       |
|  LTE+ | https://coverage-map.megafon.ru/{0}/{1}/{2}.png?layers=lte_plus           | 0        | 13       | True       |
|  ALL  | https://coverage-map.megafon.ru/{0}/{1}/{2}.png?layers=2g.3g.lte.lte_plus | 0        | 13       | True       |
